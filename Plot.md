# MAJOR SPOILERS FOR THE PLAY

The Agency is secretly working on a Supervirus that can effectively wipeout the human race as we know it. They created 1 virus before, in 2019 (covid,) but it was eradicated by the scientists. They're using an A.I called Doomsday Anti-Diversion (DAD) to organize their work.


The Spies are lead by a Hacker. This Hacker had a brother who previously worked for The Agency, but died due to the virus. He developed a hack for the DAD A.I (with the help of Spy 1.) This hack, dubbed Personal Doomsday Adversion Technology (PDAT) created robots with the ability to move stealthily. It organizes these robots, orchestrating attacks.


Spy 0 (Hacker) and Spy 1 infiltrate the Agency. Slowly, Agency Operatives start getting attacked and taken hostage. But Spy 1 lets the power get to their head. They do an Evil Monologue song (The Spy and The Liar (I Expect You To Die 2.)) and accidentally do so in front of Operative 8 and Spy 0. So they have to neutralize Spy 1. 


The 2 interrogate (torture) Spy 1 for info, with Operative 8 being Good cop and Spy 0 being Bad Cop. Operative 8 gets the absolutely amazing idea to torture Spy 1 with a bit of elevator music, but as they start to leave, Spy 1 gets "kidnapped" by the Robots. This causes Operative 19 (Hacker) to be promoted to Right Hand Man status. And a Meeting gets called.


During the meeting, all the Operatives get ambushed by the Robots, and an epic Fight Scene commenses. Amidst all the chaos, Spy 0 integrates the PDAT system into DAD. The fight is over. Spy 0 does a "Status Check," which announces there are "4 operatives active." But there's 5 people in the room. Spy 0 does a "Security Audit," which disables the Security. It's now their turn to do a evil monologue, singing Cog In The Machine (I Expect You To Die 3.) 


He neutralizes 3 operatives, leaving Operative 8, and Him. He steals the Supervirus out of Operative 8's hands, and explains what it is and his motives. But DAD updates. Taking control ov the PDAT module under Operative 0's command. Thinking Operative 8 is safe, he destroys one of the bots holding 8 in place, and tells him to run. Hacker fights the remaining Robots, but gets Tazed by 8 at the end of the song (Honeymoon Phase.) 


Operative 8 reveals to Hacker that he's Operative 0, and tries to neutralize Hacker. But Hacker places a binding agent on 0. If Hacker dies, 0 dies. The 2 talk for a bit, Hacker trying to talk sense into 0, but failing. But there's 1 saving grace. Spy 1, commanding a robot, goes up to Hacker, which realizes. He tells Spy 1 to neutralize him. So he does. "I'm sorry, Brother. I changed a lot from the last time you saw me," are Spy 1's final words as he leaves.
# Battle Of Betrayal

Battle of Betrayal is a play about 2 factions, The Agency, and The Spies. The Spies are trying to steal the Agency's top secret work, using Robots to cripple the Agency's forces. These spies act as Impostors among the Agency's forces, sabotaging them when they get the chance.

## The Cast

Operatives 1-7 & 9-18
Operative 8, AKA Operative 0 (leader)
Spies 1 & 2
Robots

